import { Component, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ActionSheetController, ModalController } from 'ionic-angular';
import { Dish } from '../../shared/dish';
import { Comment } from '../../shared/comment'; 
import { SocialSharing } from '@ionic-native/social-sharing';

import { FavoriteProvider } from '../../providers/favorite/favorite';
import { CommentPage } from '../comment/comment'; 

/**
 * Generated class for the DishdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dishdetail',
  templateUrl: 'dishdetail.html',
})
export class DishdetailPage {
  dish: Dish;
  errMess: string;
  avgstars: string;
  numcomments: number;
  favorite: boolean = false; 

  constructor(public navCtrl: NavController, public navParams: NavParams,
    @Inject('BaseURL') private BaseURL, private favoriteservice: FavoriteProvider,
    private toastCtrl: ToastController, private actshtCtrl: ActionSheetController,
     private modalCtrl: ModalController, private ss: SocialSharing) {
    
    
    this.dish = navParams.get('dish');
    this.numcomments = this.dish.comments.length;
    this.favorite = this.favoriteservice.isFavorite(this.dish.id)
    let total = 0;
    this.dish.comments.forEach(comment => total += comment.rating );
    this.avgstars = (total/this.numcomments).toFixed(2);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DishdetailPage');
  }
  
  addToFavorites() {
  
    this.favorite = this.favoriteservice.addFavorite(this.dish.id)
    this.toastCtrl.create({
        message: "Dish " + this.dish.id + " has been added to favorites",
        position: "middle", 
        duration: 3000
    }).present();
  }
  
  popOver() {
    
    let popover = this.actshtCtrl.create({
    
        title: 'Action',
        buttons: [
       {
         text: 'Add to Favorites',
         handler: () => {
           this.favorite = this.favoriteservice.addFavorite(this.dish.id)
            this.toastCtrl.create({
                message: "Dish " + this.dish.id + " has been added to favorites",
                position: "middle", 
                duration: 3000
            }).present();
         }
       },
       {
         text: 'Add a Comment',
         handler: () => {
           let modal = this.modalCtrl.create(CommentPage); 
           modal.onDidDismiss(data => {
            this.dish.comments.push(data); 
            });
            modal.present();

         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       },
       {
         text: 'Share via Facebook',
         handler: () => {
            this.ss.shareViaFacebook(this.dish.name + ' -- ' + 
            this.dish.description, this.BaseURL + this.dish.image, '')
            .then(() => console.log('Posted on Facebook'))
            .catch(() => console.log('Failed to post on Facebook')); 
          }
        },
        {
          text: 'Share via Twitter',
          handler: () => {
            this.ss.shareViaTwitter(this.dish.name + ' -- ' + this.dish.description, this.BaseURL + this.dish.image, '')
              .then(() => console.log('Posted successfully to Twitter'))
              .catch(() => console.log('Failed to post to Twitter'));
          }
       }
     ]

    });
    popover.present(); 
  }
  
  
  
}
